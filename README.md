# Free-Droid

![screenshot-working](uploads/screenshot-working.png)

Free-Droid is an application designed to help you install custom roms on your Android device. It is supposed to make the process of finding, downloading and installing a rom (with addons like Magisk, F-Droid and microG) as easy as possible. The primary audience is all the folks who do not really know anything about Android, TWRP or roms, but still would like to run a free, open-source and non-bloated Android on their devices. Using a free after-market rom like LineageOS should not be restricted to the few people with enough technical know-how to do the complex installation procedure all by themselves.

Free-Droid is available for Windows, Mac and Linux. As of now, it should be working fine with Samsung, OnePlus, Motorola, NVidia and Sony devices and with all other devices that already have TWRP installed.


Table of Contents
=================

* [How it works](#how-it-works)
* [Usage](#usage)
   * [Start](#start)
   * [Settings](#settings)
   * [Expert settings](#expert-settings)
* [Help](#help)
* [The help section is not helping](#the-help-section-is-not-helping)
* [Usage statistics](#usage-statistics)
* [I want to help](#i-want-to-help)
* [Uninstall](#uninstall)
* [Developer guide](#developer-guide)
* [Acknowledgements](#acknowledgements)

## How it works

Free-Droid needs your device to be plugged in with USB and to have USB-Debugging enabled in the Android developer settings. Once Free-Droid detects your device, it will start looking for available TWRP and roms. TWRP is a so-called recovery system that is used to install roms and therefore needs to be installed prior to rom installation. Free-Droid will only allow you to click the start button if everything needed actually is available for your device.  
When started, Free-Droid will first download all the files it needs. They will preferably come from official sources and will be checked against md5 and/or sha256 checksums. Normally, this should boil down to TWRP, the rom itself and (if left to default settings) Magisk, F-Droid, MicroG and the Aurora Store (the last three of which are bundled into the NanoDroid package). If your device does not seem to already run a custom rom, the second step will be the installation of TWRP. If needed, it will try to guide you through the process of unlocking your bootloader. After that, if we were able to boot the device into TWRP, Free-Droid will check if the data partition is mountable, because it will need to be wiped (or formatted) for the new rom to run as expected. Finally, Free-Droid will go through the installation of the rom with the additional extras like Magisk and reboot your device to the new rom when finished.

## Usage

### Start

![screenshot-start](uploads/screenshot-start.png)

For the start button to become available, you will need to confirm that you have backups of what you need and to connect your device with debugging enabled over a USB cable to your computer.
Connecting the device with USB should not be too hard, but if you need help enabling debugging in the developer settings, you can click on the hint at the bottom and enter your device name. Normally, to enable the developer settings(hidden by default) on your device, you have to open the settings, then go to "about phone" and tap 7 times on "build number". After that, the developer settings become available in your settings and you can enable "android debugging".

IMPORTANT: If you have an option to enable "OEM unlock" in your developer settings, you *must* do so!

### Settings

On the settings panel of Free-Droid, you can change some default settings to your liking. Most prominently, you may choose which rom to install - though there are a lot of devices for which only one rom is going to be available. Free-Droid looks for official releases of LineageOS, LineageOS for MicroG, Resurrection Remix, Omnirom, CarbonROM and AOSP-Extended. Additionally, it looks for roms available from the unofficial Free-Droid archive holding roms mainly for devices with no official releases but with roms found to be working just fine on XDA.  
With the other settings you can choose whether to install the listed extras: Magisk, MicroG, F-Droid and Aurora Store. For more information on these, visit the websites of the individual projects.

![screenshot-settings](uploads/screenshot-settings.png)

On this page of the application you can change some basic settings:

- Choose your rom: this is where you may select one of the available systems (rom) for installation. If available, LineageOS will be default, because it has the hardest requirements for a rom to be officially released for a specific device on their website. For the user this means that a stable quality rom can be expected. Still, other roms most likely are as good and stable as LineageOS, since they often are based on LineageOS. Notice that the Android version of the selected rom is displayed, so you can take that into consideration when deciding which rom to choose. Free-Droid looks for official releases of LineageOS, Resurrection Remix, Omnirom, CarbonROM and AOSP Extended.

- Unofficial archive: The unofficial archive is a private rom archive of Free-Droid. It is mainly meant to make roms available for devices with no rom available from official sources like the LineageOS website. If a rom is available from the unofficial archive, this checkbox will be enabled. Keep in mind, that downloads from the unofficial archive may be very slow, since the Free-Droid project has no money to pay for a server with a fast internet connection.

- Use a zip file: If you are sure about what you are doing, you may want to install a rom, that you downloaded by yourself from somewhere like XDA as a zip file to your computer. In that case, tick this checkbox and select the zip file you want to install.

- Install Magisk: Magisk is a very comfortable solution if you want to have full administration rights (root) on your Android. This is needed to apply changes to the system from within the system, but comes with the risk of breaking things if you do not really know what you are doing. If you do not know what root privileges are good for, you can also choose not to install Magisk.

- Install MicroG: MicroG replaces the Google Play Services and the Google Apps normally preinstalled on every Android device. It is needed for some non-free applications to work properly on a rom without the real Google stuff installed. It also allows your device to find its location by using wifi and cell towers in concordance with the open Mozilla database instead of the Google ones (needs to be configured in the MicroG settings after the installation is done).

- Install F-Droid: This is *the* app-store for free and open-source Android apps. It contains replacements for any non-free apps that you might use on a daily basis. It also offers apps that the Google Play Store will refuse to list, like adaway and newpipe and has purely free and open-source versions of apps that otherwise contain some non-free or closed-source components like Telegram and Maps.Me.

- Install Aurora Store: This is an app made to use the Google Play Store without having the original Play Store app installed. You can use it with your own Google account or without Google account. Note: If using it without Google account, you will not be able to install paid apps.


### Expert settings

It is advised to only change these settings if you really know what they mean and what you are doing. Otherwise the result of the installation process might not be what you would expect.

![screenshot-expert](uploads/screenshot-expert.png)

You should only change these settings if you really know what you are doing or if you have been instructed to do so!

- Only flash selected rom/zip: Skip all installation steps except the installation of the selected rom or zip file. It will not skip flashing TWRP first if no custom rom (or running TWRP) is detected.

- Signature spoofing patch: Flash the signature spoofing patch provided by the NanoDroid project. It tries to enable signature spoofing on roms that have no native signature spoofing support. This is needed for all of MicroG's features to work correctly.

- Install swype: Install the closed-source libraries enabling the swype feature on the Android virtual keyboard. Keep in mind that you use the same virtual keyboard to enter all sorts of passwords and login credentials!

- Install google sync adapters: Check this if you use your Google account to synchronize your contacts and calendars.

- Skip unlocking bootloader: (Irrelevant for Samsung devices!) If no custom rom or running TWRP is detected, Free-Droid will attempt to unlock your bootloader before starting the installation of TWRP. Note that you *need* an unlocked bootloader to install TWRP. Use this setting to skip the bootloader unlocking procedure.

- Force reflash TWRP first: If a custom rom or a running TWRP is detected, Free-Droid will automatically assume that the bootloader is already unlocked and a version of TWRP is already installed. In that case, these two steps will be skipped. Use this settings to make Free-Droid reflash TWRP before proceeding with the rom installation.

- Use a TWRP image file: You can select a TWRP image file from your computer to be used instead of retrieving a TWRP image from the official TWRP releases or from the Free-Droid unofficial archive. If you select this option, Free-Droid will not try to update TWRP.

- Do not wipe data partition: By default, Free-Droid will always wipe the data partition and perform a so-called clean flash of the rom. Not wiping the data partition most certainly causes problems in the system after the installation. Use this to perform a "dirty flash" if you know what you are doing.


## Help

![screenshot-help](uploads/screenshot-help.png)

### Device not detected

A few things first:
- Try another USB port on your computer
- Try another USB cable
- Try to install official drivers
- Restart the computer

#### Windows
If Free-Droid is unable to detect your device, you might need to install the official Windows drivers from the manufacturer of your device. Click [here](https://developer.android.com/studio/run/oem-usb#Drivers) to find where to download the driver for your device.

## The help section is not helping

Don't panic. A lot of different problems can arise and just very few of them are critical. If the installation worked but the Android system has unwanted bugs, you could retry the installation and choose not to install Magisk for example.  
If you want to know more about the problem, you can take a look at the log files created in the Free-Droid folder. Also, feel free to send an email or open an issue on GitLab and attach your log files.


## Usage statistics

By default, Free-Droid collects some very useful information about how it is being used. These information are of course completely anonymous and consist of your device model and the installation success. The latter is reported as simply successful or for example as failed at rom installation.
The collection of used devices and success rate is important, because it will reveal what devices Free-Droid works well with, what devices it does not work with (yet) and how to make Free-Droid more compatible with your devices.  

Click [here](https://stats.free-droid.com/index.php?module=Widgetize&action=iframe&secondaryDimension=eventAction&disableLink=0&widget=1&moduleToWidgetize=Events&actionToWidgetize=getCategory&idSite=3&period=range&date=2019-07-01,today&disableLink=1&widget=1) to view the collected data!


## I want to help

Awesome! Help is needed especially for increasing the number of compatible devices. When Free-Droid detects a device, it will ask for its model name and then lookup a codename used to find the correct TWRP and roms. For this to work reliably, the [lookup file](lookups/codenames.yml) mapping model names like "Moto G 2015" to a codename like "osprey" will need to be updated.  
If your device is not (or wrongly) recognized by Free-Droid, please get in touch to update the lookup file with your device model name. If the installation of TWRP does not work, we might need to update another lookup file containing the [names of the partition](lookups/recovery_partition_names.yml) TWRP needs to get installed to. This is another aspect where help might be needed to make Free-Droid compatible with your device. In the same way, we need to keep a third lookup file updated. [This one](lookups/recovery_key_combinations.yml) contains instructions on how to reboot a specific device to recovery with a combination of hardware keys to press, hold or switch. Finally, if there is just no rom available for your device, we probably can upload an unofficial release to the archive. In that case, please get in touch and tell what device will need an unofficial rom - and if you have a specific rom in mind, please leave a link to the XDA thread of the unofficial release to be uploaded to the archive.


## Uninstall

Free-Droid does not need any installation or uninstallation. If you wish to get rid of it, just remove the folder containing Free-Droid.


## Developer guide

Install jruby and install the gems shoes4, mechanize, jruby-openssl, rubyzip, sentry-raven, pry and pry-debugger-jruby.

You can run the application with a simple `jruby gui.rb`. Use `jruby --debug gui.rb -dev` to run the application in developer mode, which means that you will get the pry command prompt from inside the application in your terminal window.

If you want to package the application to a jar file, it should be enough to run `shoes package --jar ./gui.rb`. Keep in mind that shoes only runs on java 7 and 8. For this and for convenience reasons, I bundle a JRE in the packaged releases of Free-Droid.

If you only want to quickly try with a few lines of different code, you can modify the rb files inside the jar, update the jar with the modified files and relaunch the application.


## Acknowledgements

Free-Droid would not be possible without the work of the following projects:

[LineageOS](https://lineageos.org),
[ResurrectionRemix](https://www.resurrectionremix.com),
[Omnirom](https://www.omnirom.org),
[CarbonROM](https://carbonrom.org),
[AOSP-Extended](https://aospextended.com),
[TWRP](https://twrp.me),
[MicroG](https://microg.org),
[F-Droid](https://f-droid.org),
[Aurora](https://gitlab.com/AuroraOSS/AuroraStore),
[Magisk](https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445),
[NanoDroid](https://gitlab.com/Nanolx/NanoDroid).

... and of course, the amazing [XDA](https://www.xda-developers.com/) community. Thank you!
