#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module to handle Android Debug Bridge shell commands

require_relative "os"
require_relative "execute"
include Execute


module ADB
extend Log  # make log() method available as shortcut for Log.add()
class << self

  def reboot(target = "android")
    case target.downcase
    when "recovery", "bootloader", "sideload", "sideload-auto-reboot", "download" then
      execute "#{adb_command} reboot #{target.downcase}"
    else execute "#{adb_command} reboot"
    end
  end

  def get_state
    state = execute("#{adb_command} get-state", "ALL")
    case
    when state[0].to_s.strip == "sideload" then "sideload"
    when booting?(state) then "booting"
    when state[0].to_s.strip == "device" then "android"
    when state[0].to_s.strip == "recovery" then "recovery"
    when state[1] && state[1].to_s.start_with?("error: device unauthorized") then "unauthorized"
    else nil
    end
  end

  alias state get_state

  def connected?
    get_state ? true : false
  end

  def sideload?
    state == "sideload" ? true : false
  end

  def recovery?
    state == "recovery" ? true : false
  end

  def android?
    state == "android" ? true : false
  end

  def unauthorized?
    state == "unauthorized" ? true : false
  end

  def ready?
    case get_state
    when "android" then boot_complete?
    when "recovery" then true
    else false
    end
  end

  # avoid creating a recursive loop between booting? and get_state
  def booting?(state = nil)
    state = execute("#{adb_command} get-state", "ALL") if state.nil?
    # sleep 0.1 # app crashed inside this method sometimes... maybe it's better now
    return false unless state[0]
    !boot_complete? && state[0].strip == "device" ? true : false
  end

  def boot_complete?
    result = execute "#{adb_shell_command} getprop dev.bootcomplete"
    result.strip == "1" ? true : false
  end

  def wait_for_boot_complete(max = 30)
    max.times do
      return true if ready?
      sleep 1
    end if booting?
  end

  def wait_for_me(max = 30)
    # Try to reboot if fastboot or heimdall is available
    Fastboot.reboot if Fastboot.connected?
    Heimdall.reboot if Heimdall.connected?
    max.times do
      return true if ready?
      sleep 1
    end
  end

  alias wait_for_adb wait_for_me

  def sideload(zip = "update.zip")
    return false if ! File.exist?(zip)
    if wait_for_sideload
      sleep 3
      result = execute "#{adb_command} sideload #{zip}", "STATUS"
      sleep 10
      result
    else
      false
    end
  end

  def wait_for_sideload
    return true if sideload?
    recovery? ? twrp_open_sideload : false
  end

  def twrp_wipe(partition)
    case partition.to_s
    when "cache" then result = execute("#{adb_shell_command} twrp wipe cache")
    when "dalvik" then result = execute("#{adb_shell_command} twrp wipe dalvik")
    when "data" then result = execute("#{adb_shell_command} twrp wipe data")
    when "system" then result = execute("#{adb_shell_command} twrp wipe system")
    else return false
    end
    result.start_with?("E:") ? false : true
  end

  def twrp_open_sideload
    execute("adb shell twrp sideload", "STATUS")
  end

  def dd(input_file, output_file)
    return false unless whoami == "root"
    execute("#{adb_shell_command} dd if=#{input_file} of=#{output_file}").strip
  end

  def whoami
    execute("#{adb_shell_command} whoami").strip
  end

  def root?
    execute("#{adb_shell_command} getprop persist.sys.root_access").strip
  end

  def imei
    if major >= 5
      imei = execute "#{adb_shell_command} service call iphonesubinfo 1"
      imei = imei.scan(/\d\./).join.scan(/\d/).join.strip
    else
      imei = execute "#{adb_shell_command} dumpsys iphonesubinfo"
      imei = imei.scan(/\d{15,}/).join.strip
    end
    imei.empty? || imei.length < 13 ? nil : imei
  end

  def sdk
    execute("#{adb_shell_command} getprop ro.build.version.sdk").strip.to_i
  end

  def release
    execute("#{adb_shell_command} getprop ro.build.version.release").strip
  end

  def major
    release.chomp.split('.').first.to_i
  end

  def serial
    execute("#{adb_shell_command} getprop ro.serialno").strip
  end

  def name
    result = execute("#{adb_shell_command} getprop ro.product.model").strip
    if !result || result.empty?
      result = execute("#{adb_shell_command} getprop ro.omni.device").strip
    end
    result
  end

  def brand
    result = execute("#{adb_shell_command} getprop ro.product.brand").strip
    if result.empty?
      result = execute("#{adb_shell_command} getprop ro.product.manufacturer").strip
    else
      result
    end
  end

  # query a specific property from adb getprop, e.g. "ro.product.model"
  def getprop(prop = nil)
    if prop
      execute("#{adb_shell_command} getprop #{prop}").strip
    else
      execute("#{adb_shell_command} getprop").strip
    end
  end

  def push(local, remote = "/sdcard/")
    execute "#{adb_command} push #{local.to_s} #{remote.to_s}", "STATUS"
  end

  def pull(remote, local = "./")
    execute "#{adb_command} pull #{remote.to_s} #{local.to_s}", "STATUS"
  end

  # def wait_for_device
  #   execute "#{adb_command} wait-for-device"
  # end
  #
  # def wait_for_recovery
  #   execute "#{adb_command} wait-for-recovery"
  # end
  #
  # def wait_for_sideload
  #   execute "#{adb_command} wait-for-sideload"
  # end
  #
  # # does not seem to work
  # def wait_for_bootloader
  #   execute "#{adb_command} wait-for-bootloader"
  # end

  def remount
    execute "#{adb_command} remount", "STATUS"
  end

  def root
    execute "#{adb_command} root", "STATUS"
  end

  def unroot
    execute "#{adb_command} unroot", "STATUS"
  end

  def orientation
    `#{adb_shell_command} dumpsys input |
     grep 'SurfaceOrientation' |
     awk '{ print $2 }'`.strip.to_i
  end

  def portrait?
    rotation = orientation
    rotation.eql?(0) || rotation.eql?(2)
  end

  def landscape?
    rotation = orientation
    rotation.eql?(1) || rotation.eql?(3)
  end

  def set_portrait
    change_accelerometer_control(0)
    res = change_device_orientation(0)
    change_accelerometer_control(1)
    res.empty? ? nil : res
  end

  def set_landscape
    change_accelerometer_control(0)
    res = change_device_orientation(1)
    change_accelerometer_control(1)
    res.empty? ? nil : res
  end

  def airplane_mode
    execute("#{adb_shell_command} settings get global airplane_mode_on").strip.to_i
  end

  def airplane_mode?
    airplane_mode.to_boolean
  end

  def enable_airplane_mode
    change_airplane_mode(1)
  end

  def disable_airplane_mode
    change_airplane_mode(0)
  end

  def monkey(package, event_count = 500)
    execute "#{adb_shell_command} monkey -p #{package} #{event_count}"
  end

  def lock
    cmd = "#{adb_shell_command} input keyevent 26"
    execute "#{cmd}"
    res = execute "#{cmd}"
    res.empty? ? nil : res
  end

  def unlock
    res = execute "#{adb_shell_command} input keyevent 82"
    res.empty? ? nil : res
  end

  def send_to_background
    res = execute "#{adb_shell_command} input keyevent 3"
    res.empty? ? nil : res
  end

  def bring_to_foreground(package, activity)
    res = execute "#{adb_shell_command} am start -n #{package}/#{activity}"
    res.empty? ? nil : res
  end

  def reset_app(package)
    res = execute "#{adb_shell_command} pm clear #{package}"
    res.empty? ? nil : res
  end

  def stop_app(package)
    res = execute "#{adb_shell_command} am force-stop #{package}"
    res.empty? ? nil : res
  end

  def uninstall_app(package)
    res = execute "#{adb_shell_command} pm uninstall #{package}"
    res.empty? ? nil : res
  end

  def start_intent(uri)
    res = execute "#{adb_shell_command} am start -a android.intent.action.VIEW -d #{uri}"
    res.empty? ? nil : res
  end

  def input_text(text)
    res = execute "#{adb_shell_command} input text #{text}"
    res.empty? ? nil : res
  end

  def take_screenshot(file_name)
    res = execute "#{adb_shell_command} screencap -p | perl -pe 's/\x0D\x0A/\x0A/g' > #{file_name}.png"
    res.empty? ? nil : res
  end

  def swipe(x1, y1, x2, y2, duration = nil)
    args = duration.nil? ? "#{x1} #{y1} #{x2} #{y2}" : "#{x1} #{y1} #{x2} #{y2} #{duration}"
    res = execute "#{adb_shell_command} input swipe #{args}"
    res.empty? ? nil : res
  end

  def bin_version
    return nil unless File.exist?(adb_command.split(" ")[-1])
    unless defined?($adb_bin_version) && defined?($adb_bin_path)
      version = execute("#{adb_command} version")
      $adb_bin_version, $adb_bin_path = version.lines[1].split(" ")[-1], version.lines[2].split(" ")[-1]
    end
    $adb_bin_version
  end

  def bin_path
    return nil unless File.exist?(adb_command.split(" ")[-1])
    unless defined?($adb_bin_version) && defined?($adb_bin_path)
      version = execute("#{adb_command} version")
      $adb_bin_version, $adb_bin_path = version.lines[1].split(" ")[-1], version.lines[2].split(" ")[-1]
    end
    $adb_bin_path
  end

  def start_server
    execute "#{adb_command} start-server"
  end

  def kill_server
    execute "#{adb_command} kill-server"
  end

  def shell(cmd)
    execute "#{adb_shell_command} #{cmd}"
  end

  private

  def change_accelerometer_control(mode)
    adb_settings_system_command('accelerometer_rotation', mode)
  end

  def change_device_orientation(orientation)
    adb_settings_system_command('user_rotation', orientation)
  end

  def adb_settings_system_command(name, value)
    command = "#{adb_shell_command} content insert"
    param1 = '--uri content://settings/system'
    param2 = "--bind name:s:#{name}"
    param3 = "--bind value:i:#{value}"

    execute "#{command} #{param1} #{param2} #{param3}"
  end

  def change_airplane_mode(mode)
    command1 = "#{adb_shell_command} settings put global airplane_mode_on #{mode}"
    command2 = "#{adb_shell_command} am broadcast"
    param1 = '-a android.intent.action.AIRPLANE_MODE'
    param2 = "--ez state #{mode.to_boolean}"

    execute "#{command1} & #{command2} #{param1} #{param2}"
  end

  def adb_shell_command
    case OS.which
    when "windows" then "#{adb_command} shell"
    when "mac" then "#{adb_command} shell"
    else "#{adb_command} shell"
    end
  end

  def adb_command
    case OS.which
    when "windows" then "bin/adb.exe"
    when "mac" then "bin/adb"
    else "sudo bin/adb"
    end
  end

end
end
