#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module to handle TWRP routines

require_relative "os"
require_relative "adb"
require_relative "execute"
include Execute
require_relative "find"


module TWRP
extend Log  # make log() method available as shortcut for Log.add()
class << self

  def connected?
    ADB.recovery?
  end

  alias recovery? connected?

  def version_connected
    retry_on_connection_loss do
      execute("#{twrp_command} version")[/(?:(\d+\.[.\d]*\d+))/]
    end
  end

  $force_twrp_update = false

  def need_update?
    return false if $chk_use_own_twrp_state
    return nil unless ADB.recovery?
    return nil if $device.name.nil? || ["", "unknown"].include?($device.name.downcase)
    begin
      target_version = Get.twrp_parse_version $manager.files_to_use[:TWRP] if $manager.files_to_use[:TWRP]
      target_version = Gem::Version.new(target_version) if target_version
      current_version = Gem::Version.new TWRP.version_connected
      log "Current TWRP version: #{current_version} - Target TWRP version: #{target_version}"
      return nil if target_version.nil? || current_version.nil?
      !(current_version == target_version)
    rescue Exception => e
      log "Failed to check if TWRP needs to update."
      log e
      nil
    end
  end

  def update(options = {})
    options = {force: false, reboot: false}.merge(options)
    if $force_twrp_update
      options[:force] = true
      $force_twrp_update = false
    end
    return false unless ADB.recovery?
    if options[:force] || need_update?
      Gui.do :update_instructions, "Installing TWRP permanently..." if options[:force]
      ### flash twrp zip on A/B devices ###
      twrp_file_path = $manager.get_twrp
      ADB.push twrp_file_path
      twrp_filename = File.basename twrp_file_path
      dd_of = Find.recovery_dev_path
      ADB.dd "/sdcard/#{twrp_filename}", dd_of
      ADB.reboot "recovery" if options[:reboot]
      true
    else true
    end
  end

  def wipe(partition)
    sleep 2
    case partition.to_s.downcase
    when "cache" then result = execute("#{twrp_command} wipe cache")
    when "dalvik" then result = execute("#{twrp_command} wipe dalvik")
    when "data" then result = execute("#{twrp_command} wipe data")
    when "system" then result = execute("#{twrp_command} wipe system")
    else return false
    end
    sleep 2
    result.start_with?("E:") ? false : true
  end

  def wipe_caches
    wipe("cache") && wipe("dalvik")
  end

  def wipe_clean
    wipe_caches && wipe("data")
  end

  def format_data
    return false unless connected?
    wait_untill_ready
    sleep 1
    data_partition_candidates = Find.data_dev_path_candidates
    success = nil # initialize success variable to return it at the end
    data_partition_candidates.each do |data_partition|
      fs = Find.data_fs
      log "Attempting to format data partition with path #{data_partition} and fs #{fs}..."
      success = if data_partition && fs
        if fs == "f2fs"
          r = ADB.shell ("mkfs.f2fs -t 0 #{data_partition}")
          log r
          r.downcase.include?("format successful")
        elsif fs == "ext4"
          r = ADB.shell ("make_ext4fs #{data_partition}")
          log r
          r.downcase.include?("created filesystem")
        else
          log "Data partition filesystem appears to be neither f2fs nor ext4. Maybe reading fs type failed?"
          false
        end
      else false
      end
      break if success
    end
    success
  end

  def sideload?
    ADB.sideload?
  end

  def open_sideload
    execute("#{twrp_command} sideload", "STATUS")
  end

  def sideload(zip_file = "update.zip")
    return false unless File.exist?(zip_file)
    if wait_for_sideload
      sleep 3
      result = execute "#{adb_command} sideload #{zip_file}", "STATUS"
      sleep 5
      if result
        true
      else
        sleep 5 while sideload?
        last_sideload_success?
      end
    else
      false
    end
  end

  def wait_for_sideload
    return true if sideload?
    open_sideload if recovery?
    sleep 5
    sideload?
  end

  def flash_rom(rom_file, wipe = "clean")
    case wipe
    when "clean" then wipe_clean
    when "dirty", "caches" then wipe_caches
    else wipe_caches  # or should it be possible to wipe absolutely nothing?
    end
    if File.file?(rom_file)
      flash_zip_generic(rom_file) || last_sideload_success?
    else
      log "Error retrieving #{rom_file}"
    end
  end

# flash a zip using sideload
  def flash_zip_generic(zip_file)
    retry_on_connection_loss do
      # If we try to flash a rom, make sure TWRP is able to flash it
      # unless running_twrp_supports_rom?(zip_file)
      #   log "Your version of TWRP is too old."
      #   return false
      # end if is_a_rom?(zip_file)
      sideload zip_file
    end
  end

  # Flash the latest available version of given rom.
  # Attempts to Find.codename if no device parameter is provided.
  def flash_latest(rom, device = "")
    device = Find.codename if device.empty?
    rom = rom.to_sym
    # check if distribution is known and implemented
    return false unless Get.distributions[rom] && rom != :TWRP
    # check if the running TWRP version supports flashing latest rom
    # return false unless running_twrp_supports_rom?(Get.available(device)[rom])
    # send NanoDroid setup file to device if we want to flash NanoDroid
    rom_file = Get.get rom, device
    wipe_clean if is_a_rom?(rom)
    if rom_file
      case rom
      when :NanoDroid
        flash_nanodroid(file: rom_file)
      else
        flash_zip_generic rom_file
      end
    else
      log "Error retrieving #{rom}"
    end
  end

  def wait_for_me(max = 30)
    return true if connected?
    # Try to reboot if fastboot or heimdall is available
    Fastboot.reboot if Fastboot.connected?
    Heimdall.reboot if Heimdall.connected?
    reboot("recovery") if ADB.android?
    # wait for max. 5 min if sideload is running
    if sideload?
      log "Waiting for sideload to finish. This might take up to 5 minutes."
      (60 * 5 / 3).times { break if connected?; sleep 3 }
    end
    sleep 5
    max.times do
      return true if connected?
      sleep 1
    end
    false
  end

  alias wait_for_recovery wait_for_me
  alias wait_for_twrp wait_for_me

  # wait for twrp to be fully booted up and ready for sideload
  def wait_untill_ready(max = 45)
    return false unless TWRP.connected?
    start = Time.now
    wait_time = -(start - Time.now).to_i
    ADB.pull("/tmp/recovery.log", "log/recovery-tmp.log")
    if File.exist? "log/recovery-tmp.log"
      loglines = File.new("log/recovery-tmp.log").readlines
      until loglines.any?{|line| line.include?("Set page: 'main")} || wait_time > max
        sleep 3
        ADB.pull("/tmp/recovery.log", "log/recovery-tmp.log")
        loglines = File.new("log/recovery-tmp.log").readlines
        wait_time = -(start - Time.now).to_i
      end
    end
    File.delete "log/recovery-tmp.log" rescue nil
  end

  def reboot(target = "android")
    ADB.reboot(target)
  end

  def is_a_rom?(zip_file)
    Get.is_a_rom? zip_file
  end

  # If rom_file is recognised as a rom, check whether the running
  #   version of TWRP supports flashing this rom version.
  #   Otherwise return true, because we can flash e.g. NanoDroid.
  def running_twrp_supports_rom?(rom_file)
    rom_file = File.basename rom_file
    return true unless is_a_rom?(rom_file)
    rom = Get.which_distribution(rom_file)
    rom_version = Get.send("#{rom.downcase}_parse_version".to_sym, rom_file)
    Get.check_compatibility version_connected, rom, rom_version
  end

  # keeping unnecessary file parameter for code coherence
  def flash_nanodroid(file = "", setup = {})
    #file_patcher = Get.get :NanoDroid_Patcher
    file_main = Get.get :NanoDroid
    #file_bromite = Get.get :NanoDroid_Bromite
    send_nanodroid_setup setup
    flash_zip_generic file_patcher if file_patcher
    flash_zip_generic file_main if file_main
    flash_zip_generic file_bromite if file_bromite
  end

  def send_nanodroid_setup(setup = {})
    # set default settings (overridden by provided hash argument)
    # set forcesystem: 1 to avoid booting into the rom in between (magisk requirement)
    setup = {microg: 1, gmscore: 0, fdroid: 1, apps: 0, play: 31,
            overlay: 0, zelda: 0, mapsv1: 1, init: 0, gsync: 0,
            swipe: 0, forcesystem: 1, nlpbackend: 1100, nano: 1,
            bash: 1, utils: 1, fonts: 0, override: 0}.merge(setup)
    File.open(".nanodroid-setup", "w") do |file|
    file.puts "nanodroid_microg=#{setup[:microg]}"
    file.puts "nanodroid_gmscore=#{setup[:gmscore]}"
    file.puts "nanodroid_fdroid=#{setup[:fdroid]}"
    file.puts "nanodroid_apps=#{setup[:apps]}"
    file.puts "nanodroid_play=#{setup[:play]}"
    file.puts "nanodroid_overlay=#{setup[:overlay]}"
    file.puts "nanodroid_zelda=#{setup[:zelda]}"
    file.puts "nanodroid_mapsv1=#{setup[:mapsv1]}"
    file.puts "nanodroid_init=#{setup[:init]}"
    file.puts "nanodroid_gsync=#{setup[:gsync]}"
    file.puts "nanodroid_swipe=#{setup[:swipe]}"
    file.puts "nanodroid_forcesystem=#{setup[:forcesystem]}"
    file.puts "nanodroid_nlpbackend=#{setup[:nlpbackend]}"
    file.puts "nanodroid_nano=#{setup[:nano]}"
    file.puts "nanodroid_bash=#{setup[:bash]}"
    file.puts "nanodroid_utils=#{setup[:utils]}"
    file.puts "nanodroid_fonts=#{setup[:fonts]}"
    file.puts "nanodroid_override=#{setup[:override]}"
    end
    # setup_file.close
    retry_on_connection_loss do
      ADB.push ".nanodroid-setup"
    end
  end

  def systest(save_to_file = nil, save_to_folder = "log/")
    wait_untill_ready
    flash_zip_generic Get.dl_nanodroid_systest[:systest]
    wait_for_me
    sleep 1
    get_log
    # Find filename of the latest systest log
    if File.exist? "log/recovery.log"
      loglines = File.new("log/recovery.log").readlines
      # find log line with "storing results in /data/media/0/nanodroid_logs"
      reverse_index = loglines.reverse.index{|e| e.include?("storing results in /data/media/0/nanodroid_logs")}
      unless reverse_index.is_a?(Integer)
        log "Error retrieving the systest logs... Here are the last 50 lines of the recovery log:"
        log loglines.last(50)
        $reporter.send_bug_report
      else
        filename = loglines.reverse[reverse_index-1].strip.split("as ")[-1]
        file_path = "log/nanodroid_logs/#{filename}"
        if File.exist?(file_path)
          FileUtils.cp file_path, "log/#{save_to_file}" if save_to_file
          File.exist?("log/#{save_to_file}") ? "log/#{save_to_file}" : file_path
        else
          nil
        end
      end
    end
  end

  def get_log(save_to_folder = "log/")
    sleep 5 while sideload?
    ADB.pull("/tmp/recovery.log", save_to_folder)
    ADB.pull("/data/media/0/nanodroid_logs", save_to_folder) ? true : ADB.pull("/sdcard/nanodroid_logs", save_to_folder)
  end

  # Get and read the TWRP log as an array of lines. last_lines specifies
  # how many lines to read. Pass 0 to last_lines to read the whole log.
  def get_and_read_log(last_lines = 0)
    get_log
    if File.exist? "log/recovery.log"
      if last_lines == 0
        File.readlines("log/recovery.log").map(&:strip)
      else
        File.readlines("log/recovery.log").map(&:strip).last(last_lines)
      end
    end
  end

  def data_mounted?
    ADB.shell("cat /proc/mounts").lines.select{|l| l.include?("/data")}[0] ? true : false
  end

  def mount_data
    if data_mounted?
      true
    else
      ADB.shell("mount /data")
      sleep 1
      data_mounted?
    end
  end

  def unmount_data
    if data_mounted?
      ADB.shell("umount /data")
      sleep 1
      ! data_mounted?
    else
      true
    end
  end

  def data_mountable?
    if data_mounted?
      true
    else
      if mount_data
        unmount_data
        true
      else
        false
      end
    end
  end

  def data_usable? # size does not report 0MB
    begin
      get_log
      !File.readlines("log/recovery.log").select{|l|
        l.start_with?("/data | /dev")}[0].strip.downcase.include?("| size: 0mb")
        # Do not confuse "backup size: 0mb" in the same line! Therefore "| size: 0mb"
    rescue Exception => e
      log e
      data_mountable?
    end
  end

  def last_sideload_success?
    last_lines_count = File.foreach("log/recovery.log").inject(0) {|c, line| c+1} if File.exist? "log/recovery.log"
    get_log
    new_lines_count = File.foreach("log/recovery.log").inject(0) {|c, line| c+1}
    last_lines_count = 0 if ![last_lines_count, new_lines_count].all? || last_lines_count >= new_lines_count
    if File.exist? "log/recovery.log"
      last = nil
      loglines = File.new("log/recovery.log").readlines
      loglines[last_lines_count..-1].reverse.each do |line|
        if line.include? "Updater process ended with"
          last = line[/Updater process ended with (.*)/, 1].strip
          break
        end
      end
      case last
      when "RC=0" then result = true
      when "ERROR: 1" then result = false
      when nil then log "Unable to parse last sideload success"; result = nil
      else log "Unknown sideload result in the TWRP log."; result = nil
      end
    else
      log "Unable to load the log of TWRP."
      result = true
    end
    result
  end

  def nanodroid_missing_space?
    if File.exist? "log/recovery.log"
      loglines = File.new("log/recovery.log").readlines
      loglines[-100..-1].any? do |line|
        line.include?("Less than 512 MB free space availabe from TWRP") ||
        line.include?("No space left on device") ||
        line.include?("not enough space available!")
      end
    else
      log "Unable to load the log of TWRP."
      false
    end
  end

  def patcher_no_work
    get_log
    if File.exist? "log/recovery.log"
      loglines = File.new("log/recovery.log").readlines
      # find log line where the current flashing cycle begins
      count = loglines.reverse.index{|e| e.include?("Framework Patcher")}
      count ||= 0 # set to 0 if count is nil or false
      loglines.last(count).any? { |line| line.include?("ROM has native signature spoofing already") }
    end
  end

  private

  # temporarily bypassing retry_on_connection_loss for testing purposes:
  def retry_on_connection_loss(try = 3, &block)
    yield
  end

  # Retries the given block and helps getting back into TWRP
  # def retry_on_connection_loss(try = 3, &block)
  #   calling_method = caller_locations(1,1)[0].label
  #   try.times do |t|
  #     # only try to wait for TWRP if we do not want to reboot anyway
  #     unless calling_method == "reboot"
  #       wait_for_recovery
  #     else
  #       # wait for connection if no connection detected
  #       if ! connected? && ! ADB.connected?
  #         30.times do
  #           break if connected? || ADB.connected?
  #           sleep 1
  #         end
  #       end
  #     end
  #     result = yield
  #     return result if result
  #     unless t == try - 1
  #       log "No connection to your device."
  #       log "You might want to reboot your device or try another cable."
  #       printf "Press Enter when your device is ready. "
  #       gets
  #       log "Retrying..."
  #     end
  #   end # try.times
  #   log "Unable to connect to your device."
  #   return false
  # end

  def adb_command
    case OS.which
    when "windows" then "bin/adb.exe"
    when "mac" then "bin/adb"
    else "sudo bin/adb"
    end
  end

  def adb_shell_command
    case OS.which
    when "windows" then "bin/adb.exe shell"
    when "mac" then "bin/adb shell"
    else "sudo bin/adb shell"
    end
  end

  def twrp_command
    case OS.which
    when "windows" then "bin/adb.exe shell twrp"
    when "mac" then "bin/adb shell twrp"
    else "sudo bin/adb shell twrp"
    end
  end

end # self
end # module
