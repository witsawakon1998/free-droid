#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative "adb"
require_relative "fastboot"
require_relative "heimdall"
require_relative "twrp"
require_relative "find"
require "observer"
require "cgi" # for URL encoding

class Device

  include Observable
  include Log

  attr_reader :name, :brand, :codename, :state, :sdk, :release, :major,
              :serial, :root, :imei, :heimdall, :fastboot, :adb, :twrp,
              :notifier, :observer, :device_identified_by_user, :twrp_version
  attr_writer :name, :codename, :brand, :device_identified_by_user

  def initialize
    @heimdall = Heimdall
    @fastboot = Fastboot
    @adb = ADB
    @twrp = TWRP
    @name = nil
    @device_identified_by_user = nil
    @codename = nil
    @brand = nil
    @sdk = nil
    @release = nil
    @major = nil
    @twrp_version = nil
    @serial = nil
    @imei = nil
    @notifier = Notifier.new
    @observer = nil
    add_observer(@notifier)
  end

  def observe
    self.state # ask for sudo under linux so the thread won't get in trouble
    @observer = Thread.new do
      last_state = nil
      loop do log_on_fail(quit: false, info: true) do
        connection_state = self.state
        if connection_state != last_state
          changed                 # notify observers
          last_state = connection_state
          notify_observers(self, connection_state)
        end
        sleep 5
      end end # log_on_fail and loop
    end
  end

  def scan
    if adb?
      @name = @device_identified_by_user || adb.name
      @codename = Find.codename(@device_identified_by_user || @name)
      @brand = adb.brand
      @sdk = adb.sdk
      @release = adb.release
      @major = adb.major
      @twrp_version = twrp.version_connected if adb.recovery?
      @serial = adb.serial
      @imei = adb.imei
    elsif @device_identified_by_user
      @name = @device_identified_by_user
      @codename = Find.codename(@device_identified_by_user)
    end
    {name: @name, codename: @codename,
    brand: @brand, sdk: @sdk, release: @release, major: @major,
    twrp_version: @twrp_version, serial: @serial, imei: @imei}
  end

  def clear
    @name = nil
    @codename = nil
    @brand = nil
    @sdk = nil
    @release = nil
    @major = nil
    @twrp_version = nil
    @serial = nil
    @imei = nil
  end

  def supported?
    log_on_fail { Find.supported? (@codename ? @codename : scan[:codename]) }
  end

  def adb?
    adb.connected?
  end

  def fastboot?
    fastboot.connected?
  end

  def heimdall?
    heimdall.connected?
  end

  def state
    return "heimdall" if heimdall?
    return "fastboot" if fastboot?
    return adb.get_state if adb?
    return nil
  end

  def connected?
    state ? true : false
  end

  def wait_for_connection(max = 30)
    max.times do
      return state if connected?
    end
    false
  end

  def wait_for_bootloader
    # check if a bootloader is already connected
    return true if heimdall? || fastboot?
    # don't know which bootloader to wait for if the brand is unknown
    if @brand.nil?
      scan
      return false if @brand.nil?
    end
    @brand.downcase == "samsung" ? wait_for_heimdall : wait_for_fastboot
  end

  def wait_for_fastboot(max = 20)
    fastboot.wait_for_me max
  end

  def wait_for_heimdall(max = 20)
    heimdall.wait_for_me max
  end

  def wait_for_adb(max = 20)
    adb.wait_for_me max
  end

  def wait_for_recovery(max = 30)
    retry_on_connection_loss do
      twrp.wait_for_me max
    end
  end
  alias wait_for_twrp wait_for_recovery

  def wait_for_usb_debugging
    log "Please activate USB-Debugging and OEM-Unlock (if applicable)"
    while true
      if wait_for_adb
        log "Device connected."
        return true if wait_for_authorization
      end
      sleep 1
    end
  end

  def wait_for_authorization
    log "Please select 'Always' and confirm the dialog on your device."
    while true
      return true if adb? && ! adb.unauthorized?
      sleep 1
    end
  end

  def reboot(target = "android")
    case state
    when "heimdall" then heimdall.reboot
    when "fastboot" then fastboot.reboot(target)
    when "android", "recovery", "booting" then adb.reboot(target)
    when "sideload" then
      log "Cannot reboot when in sideload mode. Please wait."
      reboot(target) if twrp.wait_for_me
    else "No connection to your device."
    end
  end

  def unlock
    $bootloader_unlock_code = nil
    return false unless know_how_to_unlock?
    unlocker = "#{@codename}/unlock.rb"
    File.exist?(unlocker) ? load(unlocker) : send("unlock_#{@brand.downcase}")
  end

  # make sure we are still in fastboot, otherwise return to fastboot
  def still_ready_to_unlock?
    unless connected?
      Gui.do :alert, "Please connect your device."
      sleep 2 while !connected?
      sleep 3
    end
    unless wait_for_fastboot
      Gui.do :update_instructions, "Attempt to unlock the bootloader\nfailed.\n\nPlease restart Free-Droid and try\nagain."
      return false
    end
    true
  end

  def unlock_generic
    return false unless wait_for_fastboot
    $device.fastboot.unlock "no unlock code needed"
    Gui.do :update_instructions, "1. Step: Bootloader unlock.\n\nPlease confirm YES on your device."
  end

  def unlock_motorola
    Gui.do :update_instructions, "1. Step: Bootloader unlock.\n\nYou will be redirected to a\nFree-Droid Website. Please\nfollow the instructions there."
    return false unless wait_for_fastboot
    unlock_data = Fastboot.get_unlock_data
    OS.open_browser "https://help.free-droid.com/unlock-motorola/index.html?code=#{CGI.escape(unlock_data)}"
    Gui.do :ask_for_bootloader_unlock_code
    sleep 1 while $bootloader_unlock_code.nil?
    return false unless $bootloader_unlock_code
    return false unless still_ready_to_unlock?
    $device.fastboot.unlock $bootloader_unlock_code
  end

  def unlock_sony
    imei = ADB.connected? ? ADB.imei : nil
    if imei.nil?
      ADB.shell "am start -n com.android.settings/com.android.settings.deviceinfo.ImeiInformation"
      before_message = "Please note your IMEI from your device screen.\n\nOn the Sony website, select your device, enter your IMEI,\n\ncheck the two boxes below and submit to get your unlock code."
    else
      before_message = "Your IMEI is: #{imei}. Please note it somewhere.\n\nOn the Sony website, select your device, enter your IMEI,\n\ncheck the two boxes below and submit to get your unlock code."
      Gui.do :update_instructions, "Your IMEI: #{imei}"
    end
    Gui.do :ask_for_bootloader_unlock_code, before_message, "https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/#unlock-code"
    sleep 1 while $bootloader_unlock_code.nil?
    return false unless $bootloader_unlock_code
    return false unless still_ready_to_unlock?
    $device.fastboot.unlock $bootloader_unlock_code
  end

  def unlock_oneplus
    unlock_generic
  end

  def unlock_nvidia
    unlock_generic
  end

  def know_how_to_unlock?
    return false unless @brand
    # case @brand.downcase
    # when "samsung", "motorola", "oneplus", "nvidia", "sony" then true
    # else false
    # end rescue nil
    Find.class_variable_get(:@@supported_brands).include?(@brand.downcase) rescue nil
  end

  ##### unfinished code below #####
  # returns the path of the potential download plugin for this device
  def download_plugin
    # "#{@codename}/#{@codename}_download.rb" unless @codename.nil?
    "plugins/#{@codename}/download.rb" unless @codename.nil?
  end

  # returns the path of the potential recovery flashing plugin for this device
  def recovery_plugin
    # "#{@codename}/#{@codename}_flash_recovery.rb" unless @codename.nil?
    "plugins/#{@codename}/flash_recovery.rb" unless @codename.nil?
  end

  # returns the path of the potential rom flashing plugin for this device
  def rom_plugin
    # "#{@codename}/#{@codename}_flash_rom.rb" unless @codename.nil?
    "plugins/#{@codename}/flash_rom.rb" unless @codename.nil?
  end

  # check if there is a recovery flashing plugin for this device
  def recovery_plugin?
    if @codename.nil?
      scan
      return false if @codename.nil?
    end
    File.exist?(recovery_plugin)
  end

  # check if there is a rom flashing plugin for this device
  def rom_plugin?
    if @codename.nil?
      scan
      return false if @codename.nil?
    end
    File.exist?(recovery_plugin)
  end
  ##### unfinished code above #####

  # boot into bootloader and flash the given recovery image
  def flash_recovery(img_file)
    unless File.exist?(img_file)
      log "#{img_file} not found."
      return false
    end
    # log "callers: #{caller_locations(1,1).each{|e| puts e.label}}"
    # result = exec_block do # bypass retry_on_connection_loss because it gets stuck somewhere
    result = retry_on_connection_loss do
      case state
      when "heimdall", "fastboot" then
        if recovery_plugin?
          # execute the plugin code (if any given) as if it was written here
          eval File.read recovery_plugin
        else
          # Pass partition name to flash method if the partition name can be found
          partition = Find.recovery_partition_name($device.codename)
          if partition
            flash_recovery_generic(img_file, partition)
          else
            flash_recovery_generic(img_file)
          end
        end
      # if device is in adb mode then reboot to bootloader and retry
      # make wait_for_bootloader false to trigger a retry # only works with real retry_on_connection_loss method
      # when "recovery", "android" then false if wait_for_bootloader
      when "recovery", "android" then
        flash_recovery(img_file) if wait_for_bootloader
      # return false is device is not connected in the first place.
      else false
      end # case
    end # retry_on_connection_loss / exec_block
    if result
      log "Successfully flashed recovery."
      true
    else
      log "Failed flashing recovery."
      false
    end
  end

  def flash_recovery_generic(img_file, partition = nil)
    case state
    when "heimdall" then heimdall.flash_recovery_generic(img_file, partition)
    when "fastboot" then fastboot.flash_recovery(img_file, partition)
    else false
    end
  end

  def flash_rom(rom_file, wipe = "clean")
    retry_on_connection_loss do
      case state
      when "recovery" then
        if rom_plugin?
          # execute the plugin code as if it was written here
          eval File.read rom_plugin
        else
          twrp.flash_rom(rom_file, wipe)
        end
      # if device is in adb mode then reboot to recovery and retry
      # make wait_for_recovery false to trigger a retry
      when "android" then false if wait_for_recovery
      # return false if device is not connected in the first place.
      else false
      end # case
    end # retry_on_connection_loss
  end

  def flash_nanodroid(zip_file, setup = {})
    retry_on_connection_loss do
      case state
      when "recovery" then
        twrp.send_nanodroid_setup setup
        twrp.flash_zip_generic(zip_file)
      # if device is in adb mode then reboot to recovery and retry
      # make wait_for_recovery false to trigger a retry
      when "android" then false if wait_for_recovery
      # return false if device is not connected in the first place.
      else false
      end # case
    end # retry_on_connection_loss
  end

  def flash_zip(zip_file)
    retry_on_connection_loss do
      case state
      when "recovery" then twrp.flash_zip_generic(zip_file)
      # if device is in adb mode then reboot to recovery and retry
      # make wait_for_recovery false to trigger a retry
      when "android" then false if wait_for_recovery
      # return false if device is not connected in the first place.
      else false
      end # case
    end # retry_on_connection_loss
  end

  # def flash_recovery_old(img_file)
  #   retry_on_connection_loss do
  #     case state
  #     when "heimdall" then heimdall.flash_recovery(img_file)
  #     when "fastboot" then fastboot.flash_recovery(img_file)
  #     # if device is in adb mode then reboot to bootloader and retry
  #     when "recovery", "android" then
  #       # don't know which bootloader to wait for if the brand is unknown
  #       if @brand.nil?
  #         scan
  #         return false if @brand.nil?
  #       end
  #       reboot "bootloader"
  #       if @brand.downcase == "samsung"
  #         wait_for_heimdall ? heimdall.flash_recovery(img_file) : false
  #       else wait_for_fastboot ? fastboot.flash_recovery(img_file) : false
  #       end
  #     # return false is device is not connected in the first place.
  #     else false
  #     end
  #   end
  # end

  def custom_rom?
    case
    when TWRP.connected? then true
    when ADB.getprop("ro.build.type") == "userdebug" then true
    when ADB.getprop("ro.build.flavor").include?("userdebug") then true
    when ADB.getprop("ro.build.display.id").include?("userdebug") then true
    else false
    end
  end

  private

  # bypass retry_on_connection_loss and just execute given block
  def exec_block(&block)
    yield
  end

  # temporarily bypassing retry_on_connection_loss for testing purposes:
  def retry_on_connection_loss(try = 3, &block)
    yield
  end

  # retry the given block depending on its return value
  # def retry_on_connection_loss(try = 3, &block)
  #   if connected?
  #     result = yield
  #     return result if result
  #   end
  #   if try.zero?
  #     log "Unable to connect to your device."
  #     return false
  #   end
  #   unless wait_for_connection 10
  #     log "No connection to your device."
  #     log "You might want to try another cable."
  #     printf "Press Enter to retry. "
  #     gets
  #   end
  #   wait_for_adb if adb.booting?
  #   retry_on_connection_loss(try - 1, &block)
  # end

end
