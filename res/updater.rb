#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module for self-updating this application

module Updater
extend Log  # make log() method available as shortcut for Log.add()
class << self

  def new_version_available?
    @@local = VERSION
    # @@remote = YAML.load(open("https://update.free-droid.com/update.yml").read)["version"]
    Get.retry_on_http_connection_problem do
      @@remote = YAML.load(open("https://gitlab.com/free-droid/free-droid/raw/master/update/update.yml").read)["version"]
    end
    Gem::Version.new(@@remote) > Gem::Version.new(@@local)
  end

  def try_update(try = 0)
    return false unless new_version_available?
    Get.retry_on_http_connection_problem do
      # update_href = "https://update.free-droid.com/Free-Droid-#{@@remote}.jar"
      # update_script_href = "https://update.free-droid.com/Free-Droid-#{@@remote}-update-script.rb"
      update_href = "https://gitlab.com/free-droid/free-droid/raw/master/update/Free-Droid-#{@@remote}.jar"
      update_script_href = "https://gitlab.com/free-droid/free-droid/raw/master/update/Free-Droid-#{@@remote}-update-script.rb"
      bin_dir = Find.bin_dir
      Get.download(update_href, "#{bin_dir}/Free-Droid.new", progressbar: false, title: "Updating...")
      # also verify checksums of update files
      unless Get.verify "#{bin_dir}/Free-Droid.new", update_href
        File.delete "#{bin_dir}/Free-Droid.new"
        log "Checksum verification of the updated version failed."
        return try_update(try + 1)
      end
      if Get.remote_file_exist? update_script_href
        Get.download(update_script_href, "update-script.rb", progressbar: false, title: "Updating...")
        unless Get.verify "update-script.rb", update_script_href
          File.delete "update-script.rb"
          log "Checksum verification of the update-script.rb failed."
          return try_update(try + 1)
        end
      end
      Gui.do :update_downloaded  # calls Updater.replace_files
    end
  end

  def update_failed
    Gui.do :update_failed
    false
  end

  def replace_files
    return false unless File.exist?("update-script.rb") || File.exist?("#{Find.bin_dir}/Free-Droid.new")
    ADB.kill_server
    if File.exist? "update-script.rb"
      log "Found update-script.rb! Executing..."
      load "update-script.rb"
    else  # standard update script
      Dir.chdir(Find.bin_dir) do
        if OS.windows?
          File.open("update.bat", "w") do |file|
            file.puts "cd %~dp0"
            file.puts "ping 127.0.0.1 -n 5"
            file.puts "ren Free-Droid.jar Free-Droid.old"
            file.puts "ren Free-Droid.new Free-Droid.jar"
          end
          sleep 0.5
          spawn("update.bat", :new_pgroup=>true)
          sleep 0.5
          exit!
        else
          begin
            File.rename("#{bin_dir}/Free-Droid.jar", "#{bin_dir}/Free-Droid.old")
            File.rename("#{bin_dir}/Free-Droid.new", "#{bin_dir}/Free-Droid.jar")
          rescue Exception => e
            log e
            File.open("update.sh", "w") do |file|
              file.puts "sleep 3"
              file.puts "mv Free-Droid.jar Free-Droid.old"
              file.puts "mv Free-Droid.new Free-Droid.jar"
            end
            FileUtils.chmod("+x", "update.sh")
            exec("./update.sh") # exits the application and launches update.sh
          end
        end
      end
    end
  end

end
end
