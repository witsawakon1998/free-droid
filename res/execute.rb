#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module to use as a shortcut for Open3.capture3

require "open3"

module Execute

  # returns array with [STDOUT, STRERR, STATUS]
  # check success with execute(command)[2].success?
  def execute(command, capture = "STDOUT")
    return nil if $raised_cannot_run_program
    begin
      result = Open3.capture3(command.to_s)
    rescue IOError => e # temporary catch missing heimdall.exe exceptions for inquiery
      if e.message.include?("Cannot run program")
        $raised_cannot_run_program = true
        #inquire
        notify_user_about_antivirus(e)
      else
        raise e
      end
    end
    result[0] = filter_adb_flood(command, result[0]) if command[/(?<=getprop ).+/] && result[0]
    case capture.to_s.upcase
    when "ALL" then result
    when "STDOUT" then result[0]
    when "STDERR" then result[1]
    when "STATUS" then result[2].success?
    end
  end

  # some roms give full adb shell getprop flood even if asking only for one property
  def filter_adb_flood(command, stdout)
    if stdout.lines.length > 10
      property_asked = command[/(?<=getprop ).+/]#.strip
      if stdout.lines.select{|e| e.include?("[#{property_asked}]")}.length == 1
        stdout.lines.select{|e| e.include?("[#{property_asked}]")}[0].strip.split(" ")[-1][1..-2] rescue ""
      else
        stdout
      end
    else
      stdout
    end
  end

  def notify_user_about_antivirus(e)
    $reporter.report url: "https://app/Antivirus"
    Gui.do :alert, "Windows Defender or another antivirus is blocking adb.exe, fastboot.exe or heimdall.exe.\n\nPlease disable it and restart or redownload Free-Droid.\n(Free-Droid is safe to use, open source and is not a virus.)"
    log e.message
    raise e
    Gui.do :close_app
    exit!
    Gui.do :exit!
  end

  def inquire
    Gui.do :antivirus_confirm, "#{e.message}\n\nDid an antivirus program say something or block\nheimdall.exe, fastboot.exe or adb.exe?"
    until $antivirus
      sleep 0.5
    end
    log e.message
    log "OS: #{OS.which}. File.exist? says: heimdall.exe: #{File.exist?("bin/heimdall.exe")}, fastboot.exe: #{File.exist?("bin/fastboot.exe")}, adb.exe: #{File.exist?("bin/adb.exe")}"
    log "User input: Did an antivirus program say something or block heimdall.exe, fastboot.exe or adb.exe? --> #{$antivirus}"
    log "User says: #{$user_idea}" if $user_idea
    raise e
    Gui.do :close_app
    exit!
    Gui.do :exit!
  end

end
